package com.example.woodpie.data;

import com.example.woodpie.entities.Book;
import com.example.woodpie.entities.User;
import com.example.woodpie.enums.ActionType;

import java.util.ArrayList;
import java.util.List;

public class FeedItem
{
    private int id;
    private User user;
    private ActionType action;
    private Book book;
    private Integer rating;
    private String review;
    private Integer likes = 0;
    private List<String> comments = new ArrayList<String>();
    private boolean liked = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ActionType getAction() {
        return action;
    }

    public void setAction(ActionType action) {
        this.action = action;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public List<String> getComments() {
        return comments;
    }

    public void setComments(List<String> comments) {
        this.comments = comments;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }
}
