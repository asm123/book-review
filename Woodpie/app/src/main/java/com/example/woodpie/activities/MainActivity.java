package com.example.woodpie.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.woodpie.R;
import com.example.woodpie.enums.IntentKeys;
import com.example.woodpie.fragments.ContentFragment;
import com.example.woodpie.fragments.FeedFragment;
import com.example.woodpie.utils.ImageDownloaderTask;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ResultCallback<People.LoadPeopleResult> {

    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;

    private GoogleApiClient mGoogleApiClient;
    private boolean mSignInClicked = false;

    private String loggedInUserName = "";
    private String loggedInUserEmail = "";
    private String loggedInUserImageURL = "";

    int profilePicWidth, profilePicHeight;

    private String TAG = MainActivity.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        profilePicWidth = (int) getResources().getDimension(R.dimen.nav_profile_pic_width);
        profilePicHeight = (int) getResources().getDimension(R.dimen.nav_profile_pic_height);

        // get user details if logged in via fb
        Intent intent = getIntent();
        if (intent.hasExtra(IntentKeys.USER_NAME.getKey())) {
            loggedInUserName = intent.getStringExtra(IntentKeys.USER_NAME.getKey());
            loggedInUserImageURL = intent.getStringExtra(IntentKeys.USER_PIC.getKey());
            setUserDetails();
        }
        else {
            Log.d(TAG, "Did not receive profile information from LoginActivity.");
            Profile profile = Profile.getCurrentProfile();
            if (profile == null) {
                Log.e(TAG, "FB profile is null.");
            }
            else {
                loggedInUserName = profile.getName();
//            loggedInUserImageURL = "http://graph.facebook.com/" + profile.getId() + "/picture";
                loggedInUserImageURL = profile.getProfilePictureUri(R.dimen.nav_profile_pic_width, R.dimen.nav_profile_pic_width).toString();
                Log.i(TAG, loggedInUserImageURL);
                setUserDetails();
            }
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();


        // loading FeedFragment by default
        FeedFragment defaultFragment = new FeedFragment();
        FragmentTransaction defaultFragmentTransaction = getSupportFragmentManager().beginTransaction();
        defaultFragmentTransaction.replace(R.id.frame, defaultFragment);
        defaultFragmentTransaction.commit();

        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if (menuItem.isChecked())
                    menuItem.setChecked(false);
                else
                    menuItem.setChecked(true);

                drawerLayout.closeDrawers();

                switch (menuItem.getItemId()) {
                    case R.id.feed_nav:
                        FeedFragment feedFragment = new FeedFragment();
                        FragmentTransaction feedFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        feedFragmentTransaction.replace(R.id.frame, feedFragment);
                        feedFragmentTransaction.commit();
                        return true;

                    case R.id.profile_nav:
                        Toast.makeText(getApplicationContext(), "Profile selected!", Toast.LENGTH_SHORT).show();
                        ContentFragment fragment = new ContentFragment();
                        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.frame, fragment);
                        fragmentTransaction.commit();
                        return true;

                    case R.id.library_nav:
                        Toast.makeText(getApplicationContext(), "Library selected!", Toast.LENGTH_SHORT).show();
                        return true;

                    case R.id.logout_nav:
                        Toast.makeText(getApplicationContext(), "Logging out!", Toast.LENGTH_SHORT).show();
                        try {
                            LoginManager.getInstance().logOut();
                            if (mGoogleApiClient.isConnected()) {
                                Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
                                mGoogleApiClient.disconnect();
                                mGoogleApiClient.connect();
                            }
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), "Problem in logging out!", Toast.LENGTH_SHORT).show();
                        }

                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                        return true;

                    default:
                        Toast.makeText(getApplicationContext(), "Something is wrong!", Toast.LENGTH_SHORT).show();
                        return true;
                }
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                toolbar, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        mSignInClicked = false;
        Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(this);

        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
            Person loggedInUser = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            loggedInUserName = loggedInUser.getDisplayName();
            loggedInUserEmail = Plus.AccountApi.getAccountName(mGoogleApiClient);
            loggedInUserImageURL = loggedInUser.getImage().getUrl();
            loggedInUserImageURL = loggedInUserImageURL.substring(0, loggedInUserImageURL.lastIndexOf("?"))
                    + "?sz=" + profilePicWidth;

            setUserDetails();
        }
    }

    private void setUserDetails() {
        ((TextView)findViewById(R.id.nav_user_name)).setText(loggedInUserName);
        ((TextView)findViewById(R.id.nav_user_email)).setText(loggedInUserEmail);
        if (loggedInUserImageURL != null) {
            new ImageDownloaderTask(((CircleImageView) findViewById(R.id.nav_user_profile_image))).execute(loggedInUserImageURL);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(People.LoadPeopleResult loadPeopleResult) {

    }

    public void likePost(View view) {
        LinearLayout parent = (LinearLayout) view;
        ImageView likeIcon = (ImageView) parent.getChildAt(0);
        TextView likeText = (TextView) parent.getChildAt(1);
        if (likeText.getText().toString().equalsIgnoreCase(getResources().getString(R.string.liked))) {
            likeText.setTypeface(null, Typeface.NORMAL);
            likeText.setText(getResources().getString(R.string.like_button_text));
            likeText.setTextColor(getResources().getColor(R.color.like_default));
            likeIcon.setImageResource(R.drawable.feed_like_default);
        }
        else {
            likeText.setTypeface(null, Typeface.BOLD);
            likeText.setText(getResources().getString(R.string.liked));
            likeText.setTextColor(getResources().getColor(R.color.like_liked));
            likeIcon.setImageResource(R.drawable.feed_like_liked);
        }
    }
}
